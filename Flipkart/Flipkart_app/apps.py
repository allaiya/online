from django.apps import AppConfig


class FlipkartAppConfig(AppConfig):
    name = 'Flipkart_app'
